//super 只能用再 ES6 語法
var parent = {
    print() {
      console.log("parent print");
    },
    valuable: "valuable of parent",
  };
  
  var child = {
    print() {
      console.log("child print");
      super.print();
      console.log(`print valuable from parent ${super.valuable}`);
      console.log(`print valuable from child ${this.valuable}`);
    },
  };
  //用Object.setPrototypeOf()來實作繼承
  //第一個參數要繼承物件，第二個參數為原型物件
  Object.setPrototypeOf(child, parent); // 要有這段才能
  child.print()
///////////////////////////////////////////////////////////////////////////////////////
  
  var child = {
      print = function () {
          console.log("child print");
          //這段執行會出錯, 因為 super 只支援ES6
          super.print();
      }
  };
//////////////////////////////////////////////////////////////////////////////////////

//class sample
class Animal {
  constructor(name) {
    this.name = name;
  }

  speak() {
    console.log(`${this.name} is barking.`);
  }
}

var a = new Animal('Yahoo');
a.speak();
//////////////////////////////////////////////////////////////////////////////////////

//使用 extend 繼承
class Animal {
    constructor(name) {
      this.name = name;
    }
  
    speak() {
      console.log(`${this.name} is barking.`);
    }
}

class Cat extends Animal {
  speak() {
    console.log(`${this.name} is MioMio.`);
  }
}
var b = new Cat("Yahoo");
b.speak();
//////////////////////////////////////////////////////////////////////////////////////

class Car {
    constructor(){
        console.log('creat a new car')
    }
}

class Tesla extends Car {
    constructor(){
        //這行一定要有
        //子類別 (sub-class) 有定義自己的 constructor，必須在 constructor 方法中顯示地調用 super()
        super();
        console.log('creat a new tesla car')
    }
} 
var t = new Tesla();
//////////////////////////////////////////////////////////////////////////////////////

class Animal {
    constructor(name){
        this.name = name;
        console.log(`${this.name} for Parent Animal`);
    }

    speak(){
        console.log(`${this.name} is barking`);
    }
}

class Cat extends Animal {
    speak(){
        super.speak();
        console.log(`${this.name} is mio mio`);
    }
}
var c = new Cat("Yahoo");
c.speak();
//////////////////////////////////////////////////////////////////////////////////////

//透過 super 調用父類別的方法時，super 會綁定子類別的 this (而不是父類別的 this)
class A {
    constructor() {
        this.x = 1;
    }

    print() {
        console.log(`${this.x}`);
    }
}

class B extends A {
    constructor() {
        super();
        this.x = 2;
    }

    foo(){
        super.print();
    }
}
var b = new B();
b.foo();
b.print();
//////////////////////////////////////////////////////////////////////////////////////

//static used
class staticMethod {
    static method1(){
        return "return method1";
    }

    static method2(){
        return this.method1() + " add method2";
    }
}
staticMethod.method1();
staticMethod.method2();
//////////////////////////////////////////////////////////////////////////////////////

//static used super
class Triple {
    static triple(number){
        return number * 3;
    }
}

class bigTriple extends Triple {
    static triple(number){
        return super.triple(number) * super.triple(number);
    }
}
Triple.triple(3);
bigTriple.triple(3);
var tp = new Triple();
tp.triple(3);
//////////////////////////////////////////////////////////////////////////////////////

//靜態屬性
class StaticMethodCall {
    static count = 0;
    static staticMethod() {
        this.count++;
        return 'Static method has been called' + this.count;
    }
    
    static anotherStaticMethod() {
        // 你可以用 this 來調用其他的 static method
        return this.staticMethod() + ' from another static method';
    }
}
StaticMethodCall.staticMethod();
StaticMethodCall.staticMethod();